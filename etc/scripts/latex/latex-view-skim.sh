#!/usr/bin/env bash
# determine where the tex main file is situated -> source the synctex.gz -> launch Skim w/ -b
# $1 - name of file ; $2 - line number ; $3 - name of the pdf ; $4 - full path to current file


mv auto/$1.synctex.gz . 2> /dev/null
/Applications/Skim.app/Contents/SharedSupport/displayline -b -g $2 $3 $4
mv $1.synctex.gz auto 2> /dev/null
