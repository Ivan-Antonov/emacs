#!/usr/bin/env bash
# $1 name of the master file

# move any previously generated files in auto directory
mv ./auto/$1.* ./ 2> /dev/null
sleep 1

echo $1

# run latexmk w/ constant update mode (pvc)
latexmk -pdf -pvc $1
