#!/usr/bin/env bash
# $1 name of the master file

# Make an auto direcotry (if doesn't exist)
mkdir -p auto 2> /dev/null

# Move computation files into that folder
ending_array=( aux bbl blg fdb_latexmk fls log out toc bcf run.xml nav snm el )

for ending in "${ending_array[@]}"
do
    temp_file="$1.$ending"
    if [ -f $temp_file ]; then
        echo "Removing '$temp_file'"
        mv -f "$temp_file" ./auto
    fi
done
