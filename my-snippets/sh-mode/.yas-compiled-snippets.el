;;; Compiled snippets and support files for `sh-mode'
;;; Snippet definitions:
;;;
(yas-define-snippets 'sh-mode
                     '(("if" "if [[ $1 ]]; then\n  $2\nfi$0" "if" nil nil nil "/Users/ivan/.emacs.d/my-snippets/sh-mode/ilya-if" nil nil)
                       ("com" "# __ PARAMETERS __\n# ${1:parameter me up}}\n#\n# __ DESCRIPTION __\n# ${2:description of function does}}" "comment" nil nil nil "/Users/ivan/.emacs.d/my-snippets/sh-mode/ilya-comment" nil nil)))


;;; Do not edit! File generated at Wed Nov 25 18:27:29 2020
