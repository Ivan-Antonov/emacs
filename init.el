(load (concat user-emacs-directory "base_configuration.el"))
(load (concat user-emacs-directory "formatting.el"))
(load (concat user-emacs-directory "orgmode.el"))
(load (concat user-emacs-directory "org-config.el"))
(load (concat user-emacs-directory "ricing.el"))
(load (concat user-emacs-directory "python.el"))
(load (concat user-emacs-directory "rust.el"))
(load (concat user-emacs-directory "dockermode.el"))
(load (concat user-emacs-directory "cppmode.el"))
(load (concat user-emacs-directory "jupytermode.el"))
(load (concat user-emacs-directory "sshmode.el"))
(load (concat user-emacs-directory "gitmode.el"))
(load (concat user-emacs-directory "markdownmode.el"))
(load (concat user-emacs-directory "javascript.el"))
(load (concat user-emacs-directory "common_lisp.el"))
(load (concat user-emacs-directory "post-load.el"))
(load (concat user-emacs-directory "latex.el"))
;; '(company-quickhelp-delay nil)
;; '(helm-follow-mode-persistent t)
;; '(package-selected-packages
;;   '(auto-complete-config zygospore yasnippet-snippets which-key wgrep web-mode volatile-highlights vagrant-tramp vagrant use-package switch-window sudo-edit string-inflection sr-speedbar smartparens rustic restart-emacs rainbow-delimiters racer quick-peek prettier-js popwin popup-kill-ring persistent-scratch ox-twbs org-bullets noflet multiple-cursors mermaid-mode lsp-ui lsp-treemacs lsp-ivy js2-mode iedit ido-vertical-mode hungry-delete htmlize hideshow-org helm-swoop helm-projectile helm-gtags helm-ag grip-mode golden-ratio git-timemachine git-gutter ggtags function-args forge flymd flycheck-rust fill-column-indicator fic-mode eyebrowse expand-region elpy elfeed ein dumb-jump doom-modeline docker-compose-mode docker diminish dashboard csv-mode counsel-projectile company-quickhelp company-lsp company-c-headers cdlatex cargo blacken beacon auto-yasnippet auctex all-the-icons-dired aggressive-indent ag add-node-modules-path ace-jump-mode))
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(company-quickhelp-delay nil)
 '(ein:jupyter-default-notebook-directory "~/")
 '(ein:output-area-inlined-images t)
 '(helm-follow-mode-persistent t)
 '(package-selected-packages
   '(company-terraform terraform-mode tide company company-ebdb zygospore yasnippet-snippets which-key web-mode volatile-highlights vagrant-tramp vagrant use-package treemacs switch-window string-inflection sr-speedbar smartparens rustic restart-emacs rainbow-delimiters prettier-js popup-kill-ring persistent-scratch ox-twbs ox-reveal org-bullets noflet multiple-cursors lsp-ui lsp-ivy iedit ido-vertical-mode hungry-delete htmlize helm-swoop helm-projectile helm-ag grip-mode golden-ratio git-timemachine git-gutter function-args forge flycheck-rust fic-mode eyebrowse exec-path-from-shell elpy ein doom-modeline docker-compose-mode docker diminish dashboard csv-mode counsel-projectile company-quickhelp company-lsp company-c-headers cdlatex cargo blacken beacon auto-yasnippet auctex all-the-icons-dired aggressive-indent ag add-node-modules-path)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((t (:family "Hasklig Light" :height 120)))))
